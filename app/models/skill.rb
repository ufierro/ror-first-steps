class Skill < ApplicationRecord
    validates :name, presence: true, length: { minimum: 1}
    validates :grading, presence: true, numericality: { other_than: 0 }
end
