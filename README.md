# First RoR app

This is just a test project that helps me familiarize with ruby and in turn rails since I know neither one of them.

On first run do (inside the root directory, wehre the docker-compose.yml file is):

```bash
# The docker-compose.yml file is pointing to a .env file; Usually these files are provided in a per-environment basis, meaning that whatever system is in charge of deploying to say, production would provide the file. As such, this file has been ignored from the repository and only a local development version is provided.
cp .env.dev .env
docker-compose up -d run web rake db:create
docker-compose run web rake db:create
docker-compose run web rails db:migrate RAILS_ENV=development
```

And visit localhost:3000 or localhost:3000/skills to see the CRUD for skills
